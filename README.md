# win32-imgui-minimal

Minimal Win32 project with IMGUI and the latest OpenGL.

This project-template makes use of the following libraries/projects:

* [IMGUI](https://github.com/ocornut/imgui/)
* [CPM](https://github.com/cpm-cmake/CPM.cmake)
* [GLAD](https://glad.dav1d.de/)
