#include <app.hpp>

#include <backends/imgui_impl_opengl3.h>
#include <backends/imgui_impl_win32.h>
#include <config.h>
#include <glad/glad.h>
#include <glad/glad_wgl.h>
#include <iostream>
#include <memory>

#define OPENGL_LATEST_VERSION_MAJOR 4
#define OPENGL_LATEST_VERSION_MINOR 6

struct WindowHandle
{
    HWND hwnd;
    HDC hdc;
    HGLRC hrc;
};

App::App(const std::vector<std::string> &args)
    : _args(args)
{}

App::~App() = default;

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplWin32_WndProcHandler(hwnd, message, wParam, lParam))
        return true;

    auto app = reinterpret_cast<App *>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

    switch (message)
    {
        case WM_NCCREATE:
        {
            SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)((LPCREATESTRUCT)lParam)->lpCreateParams);
            break;
        }
        case WM_SIZE:
        {
            if (app != nullptr)
            {
                app->OnResize(LOWORD(lParam), HIWORD(lParam));
            }

            break;
        }
        case WM_DESTROY:
        {
            if (app != nullptr)
            {
                app->OnExit();

                auto windowHandle = app->GetWindowHandle<WindowHandle>();

                if (windowHandle != nullptr)
                {
                    wglDeleteContext(windowHandle->hrc);
                    ReleaseDC(windowHandle->hwnd, windowHandle->hdc);
                }
            }

            PostQuitMessage(0);

            break;
        }
        case WM_CLOSE:
        {
            DestroyWindow(hwnd);

            break;
        }
    }

    return DefWindowProc(hwnd, message, wParam, lParam);
}

void OpenGLMessageCallback(
    unsigned source,
    unsigned type,
    unsigned id,
    unsigned severity,
    int length,
    const char *message,
    const void *userParam)
{
    (void)userParam;

    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH:
            std::cout << "CRITICAL";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            std::cout << "ERROR";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            std::cout << "WARNING";
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            std::cout << "DEBUG";
            break;
        default:
            std::cout << "UNKNOWN";
            break;
    }

    std::cout << "\n    source    : " << source
              << "\n    message   : " << message
              << "\n    type      : " << type
              << "\n    id        : " << id
              << "\n    length    : " << length
              << "\n";
}

bool App::Init()
{
    WNDCLASS wc;
    ZeroMemory(&wc, sizeof wc);

    wc.hInstance = GetModuleHandle(nullptr);
    wc.lpszClassName = APP_NAME;
    wc.lpfnWndProc = (WNDPROC)WndProc;
    wc.style = CS_DBLCLKS | CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    wc.hbrBackground = nullptr;
    wc.lpszMenuName = nullptr;
    wc.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);

    if (FALSE == RegisterClass(&wc))
    {
        return false;
    }

    auto _hwnd = CreateWindowEx(
        WS_EX_APPWINDOW | WS_EX_WINDOWEDGE,
        APP_NAME,
        APP_NAME,
        WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        0,
        0,
        GetModuleHandle(nullptr),
        reinterpret_cast<LPVOID>(this));

    if (_hwnd == nullptr)
    {
        return false;
    }

    auto _hdc = GetWindowDC(_hwnd);

    if (_hdc == nullptr)
    {
        DestroyWindow(_hwnd);

        return false;
    }

    PIXELFORMATDESCRIPTOR pfd;

    ZeroMemory(&pfd, sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 24;

    int format = ChoosePixelFormat(_hdc, &pfd);

    if (format == 0)
    {
        ReleaseDC(_hwnd, _hdc);
        DestroyWindow(_hwnd);

        return false;
    }

    if (SetPixelFormat(_hdc, format, &pfd) == FALSE)
    {
        ReleaseDC(_hwnd, _hdc);
        DestroyWindow(_hwnd);

        return false;
    }

    auto _hrc = wglCreateContext(_hdc);

    if (_hrc == NULL)
    {
        ReleaseDC(_hwnd, _hdc);
        DestroyWindow(_hwnd);

        return false;
    }

    wglMakeCurrent(_hdc, _hrc);

    gladLoadGL();

    ShowWindow(_hwnd, SW_SHOW);

    if (GLVersion.major < 3)
    {
        std::cout << "Sorry, your graphics drivers are too old to wotk with....\n";

        return false;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLMessageCallback, nullptr);

    glDebugMessageControl(
        GL_DONT_CARE,
        GL_DONT_CARE,
        GL_DEBUG_SEVERITY_NOTIFICATION,
        0,
        nullptr,
        GL_FALSE);

    int attribs[] =
        {
            WGL_CONTEXT_MAJOR_VERSION_ARB,
            OPENGL_LATEST_VERSION_MAJOR,
            WGL_CONTEXT_MINOR_VERSION_ARB,
            OPENGL_LATEST_VERSION_MINOR,
            WGL_CONTEXT_FLAGS_ARB,
            0,
            0,
        };

    auto _createContextAttribs = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");

    if (_createContextAttribs != nullptr)
    {
        auto tempContext = _createContextAttribs(_hdc, 0, attribs);

        if (tempContext != nullptr)
        {
            wglMakeCurrent(nullptr, nullptr);
            wglDeleteContext(_hrc);

            _hrc = tempContext;
            wglMakeCurrent(_hdc, _hrc);
        }
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO &io = ImGui::GetIO();
    (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplWin32_Init(_hwnd);
    ImGui_ImplOpenGL3_Init("#version 150");

    std::cout << "running opengl " << GLVersion.major << "." << GLVersion.minor << std::endl;

    SetWindowHandle<WindowHandle>(new WindowHandle({
        _hwnd,
        _hdc,
        _hrc,
    }));

    OnInit();

    return true;
}

int App::Run()
{
    MSG msg;
    bool running = true;

    auto windowHandle = std::unique_ptr<WindowHandle>(GetWindowHandle<WindowHandle>());

    if (windowHandle == nullptr)
    {
        std::cout << "Something went wrong, did you call the Init() method already?" << std::endl;

        return 0;
    }

    while (running)
    {
        while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                running = false;
            }

            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        wglMakeCurrent(windowHandle->hdc, windowHandle->hrc);

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplWin32_NewFrame();
        ImGui::NewFrame();

        OnFrame();

        ImGui::Render();

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        SwapBuffers(windowHandle->hdc);
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();

    ClearWindowHandle();

    return 0;
}
